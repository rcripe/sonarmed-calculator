$(document).ready(function() {
    var calculateResults = document.getElementById("calculateResults");
    var startOver = document.getElementById("startOver");
    var calculationsContainer = document.getElementById("calculationsContainer");
    var resultsContainer = document.getElementById("resultsContainer");
    // when "Calculate" button is clicked
    calculateResults.addEventListener("click", function(e) {
      // e.preventDefault(); // prevent form submit for now
      var form = document.getElementById('sonarMedForm');
      var error_free=true;
      //check if any required form feild is empty 
      for(var i=0; i < form.elements.length; i++){
        if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
        
          form.elements[i].classList.add("error");
          error_free=false;
        }else{
          form.elements[i].classList.remove("error");
        }
      }
      // slide up and down if error 
      if (!error_free){
        $(".alert").slideDown("slow");
        event.preventDefault();
      }else{
        $(".alert").slideUp("slow");
        //calculate savings
        calculate();
        // hide calculations container
        calculationsContainer.style.display = "none";
        //fade in results container
        $("#resultsContainer").fadeIn(400);
      }
    });
      // when "Start over" button is clicked
      startOver.addEventListener("click", function() {
      // hide results container
      resultsContainer.style.display = "none";
      // fade in calculations container
      $("#calculationsContainer").fadeIn(400);
    });

    $('#uePerMonth, #nicuDays, #estimate').keyup(function(event) {
      if($(this).val()!=''){
          $(this).removeClass("error");
      }
    });
    
});



    function ueVentDays(){
      var uePerMonth = parseInt($("#uePerMonth").val());
      var nicuDays = parseInt($("#nicuDays").val());
      var ue100 = ((uePerMonth*12)/nicuDays)*100;
      if(!ue100){return "";}
      var fValue = (Math.round(ue100 * 100) / 100).toFixed(2);
      
      $("#ue100").val(fValue);
      
      return fValue;
    }

    function noSonarMed(ue100){
        var uePerMonth = $("#uePerMonth").val();
        var unplanned = parseInt(uePerMonth)*12;
        var oneUECost = $("#oneUECost").val();
        var annual = unplanned * parseFloat(oneUECost.replace(",",""));
        return annual;

    }
    
    function withSonarMed(ue100){
      var nicuDays = parseInt($("#nicuDays").val());
      var estimate = parseFloat(($("#estimate").val())/100);
      var oneUECost = parseFloat($("#oneUECost").val().replace(",",""));
      var withSonarMed = parseFloat(ue100*(1-estimate)*(parseFloat(nicuDays)/100))*oneUECost;
      return withSonarMed;
    }

    function calculate(){
      var ue = ueVentDays();
      var savings = Math.round(noSonarMed(ue)-withSonarMed(ue));
      //format number
      $('.results__savings').text('$' + savings.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ","));
      $('#numUEs').text(ue);
    }
